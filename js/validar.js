function Enviar() {

	if (Nombre() && Correo() && Telefono()) {
		return true;
	} else {
		return false;
	}
}

function Nombre() {
	var nombre = document.getElementById('nombre');

	if (nombre.value == "") {
		alert('Nombre Vacío');
		return false;
	} else {
		
		return true;
	}
}

function Correo() {
	var miCorreo = document.formCandidato.correo.value;
	if (/\w+\@\w+\.\w+/.test(miCorreo)) {
		return true;
	} else {
		alert("La dirección de email es incorrecta.");
		return false;
	}
}

function Telefono() {
	var telefono = document.getElementById('numeroTel');

	if (telefono.value == "") {
		alert('Teléfono Vacío');
		return false;
	} else {
		return true;
	}
}
