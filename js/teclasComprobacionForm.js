// Evitar letras en el telefono
function teclaPulsada(e) {
	var keychar;
	var teclanum;

	teclanum = e.which; // codigo ASCII de la tecla pulsada
	keychar = String.fromCharCode(teclanum);

	if (keychar < "0" || keychar > "9") {
		return false;
	} else {
		return true;
	}
}

// Evitar 2 @
function comprobarArroba(e) {

	var caracterTecla;
	var codigoTecla;

	if (window.event) { // IE8
		codigoTecla = e.keyCode;
	} else if (e.which) // IE9/Firefox/Chrome/Opera/Safari
	{
		codigoTecla = e.which;
	}

	caracterTecla = String.fromCharCode(codigoTecla);

	if (caracterTecla == "@") { // Solo una @

		if (document.getElementById("emailForm").value.indexOf('@') >= 0) {
			return false;
		}
	}
}


