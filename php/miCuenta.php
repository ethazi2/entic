<!doctype html>

<html class="no-js" lang="">

<head>
<meta charset="utf-8">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Entic</title>

<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/flexslider.css">
<link rel="stylesheet" href="../css/jquery.fancybox.css">
<link rel="stylesheet" href="../css/main.css">
<link rel="stylesheet" href="../css/formu.css">
<link rel="stylesheet" href="../css/responsive.css">
<link rel="stylesheet" href="../css/font-icon.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
</head>

<body class="banner3">
	<!-- Header -->
	<section role="banner">
		<!--Menu -->
		<header id="header">
			<div class="header-content clearfix">
				<a class="logo" href="../index.html"><img src="../images/logo4.png" alt=""></a>
				<nav class="navigation" role="navigation">
					<ul class="primary-nav">
						<li><a href="../index.html">Home</a></li>
						<li><a href="../html/Curriculums.html">Curriculums</a></li>
						<li><a href="../Formulario/Formulario.html">Trabaja con nosotros</a></li>
						<li><a href="miCuenta.php">Mi Cuenta</a></li>
					</ul>
				</nav>
				<a href="#" class="nav-toggle">Menu<span></span></a>
			</div>
		</header>
		<!--Fin Menu -->

		<!-- Banner -->

		<!-- Fin Banner -->
	</section>
	<!-- Fin Header -->
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<div class="form-style-5">
		<form name="formCandidato" method="post" action="checklogin.php" onsubmit="return Enviar()">
			<fieldset>
				<legend>
					<h4 align="center">Inicio sesion</h4>
				</legend>
				<input type="text" name="username" placeholder="Nombre" id="usernames" required/> 
				<input type="password" name="password" placeholder="Contrase&ntilde;a" id="password" required />
			<input type="submit" value="Entrar" />
			</fieldset>
		</form>
	</div>
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<!-- about section -->

	<!-- Get a quote section -->
	<!-- Footer section -->
	<footer class="footer">
		<div class="footer-top section">
			<div class="container">
				<div class="row">
					<div class="footer-col col-md-6">
						<h5>Donde encontrarnos</h5>
						<p>
							Barrio Urritxe, 0 S/N, 48340 Amorebieta-Etxano, BI <br>
							contacto@entic.com
						</p>
						<p>Copyright &#169; 2015 ENTIC Inc. All Rights Reserved.</p>
					</div>
					<div class="footer-col col-md-3">
						<h5>Menu</h5>
						<p>
						<ul>
							<li><a href="../index.html">Home</a></li>
							<li><a href="../html/Curriculums.html">Curriculums</a></li>
							<li><a href="../Formulario/Formulario.html">Trabaja con nosotros</a></li>
							<li><a href="../php/miCuenta.php">Mi cuenta</a></li>
						</ul>
						</p>
					</div>
					<div class="footer-col col-md-3">
						<h5>Redes sociales</h5>
						<ul class="footer-share">
							<li><a href="https://www.facebook.com/entic.ceo/"><i
									class="fa fa-facebook"></i></a></li>
							<li><a href="https://twitter.com/EnticSoftware/"><i
									class="fa fa-twitter"></i></a></li>
									<li><a href="https://www.instagram.com/enticsoftware/"><i
									class="fa fa-instagram"></i></a></li>
							<li><a
								href="https://plus.google.com/u/2/104388675392911614666?hl=es"><i
									class="fa fa-google-plus"></i></a></li>
						</ul>
					</div>
				</div>
				<button type="button"  onclick="invertirColor()">Invertir colores</button>
			</div>
		</div>
		<!-- footer top -->
	</footer>
	<!-- Footer section -->
	<!-- JS FILES -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/jquery.flexslider-min.js"></script>
	<script src="../js/jquery.fancybox.pack.js"></script>
	<script src="../js/retina.min.js"></script>
	<script src="../js/modernizr.js"></script>
	<script src="../js/main.js"></script>
	<script src="../js/login.js"></script>
	<script src="../js/invertirColor.js"></script>
</body>
</html>