<!doctype html>

<html class="no-js" lang="">

<head>
<meta charset="utf-8">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Entic</title>

<!-- Nuestros CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/flexslider.css">
<link rel="stylesheet" href="../css/jquery.fancybox.css">
<link rel="stylesheet" href="../css/main.css">
<link rel="stylesheet" href="../css/responsive.css">
<link rel="stylesheet" href="../css/font-icon.css">
</head>

<body>
	<!-- Codigo para permitir acceso a usuarios logados solamente -->
<?php
session_start();

if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {} else {
    echo "Esta pagina es solo para usuarios registrados.<br>";
    echo "<br><a href='miCuenta.php'>Login</a>";
    echo "<br><br><a href='../index.html'>Salir</a>";
    
    exit();
}

$now = time();

if ($now > $_SESSION['expire']) {
    session_destroy();
    
    echo "Su sesion a terminado,
<a href='miCuenta.php'>Necesita Hacer Login</a>";
    exit();
}
?>
	<!-- Header -->
	<section class="bannerLog" role="banner">
		<!--Menu -->
		<header id="header">
			<div class="header-content clearfix">
				<a class="logo" href="../index.html"><img src="../images/logo4.png"
					alt=""></a>
				<nav class="navigation" role="navigation">
					<ul class="primary-nav">
						<li><a href="../index.html">Home</a></li>
						<li><a href="../html/Curriculums.html">Curriculums</a></li>
						<li><a href="../Formulario/Formulario.html">Trabaja con nosotros</a></li>
						<li><a href="miCuenta.php">Mi Cuenta</a></li>
					</ul>
				</nav>
				<a href="#" class="nav-toggle">Menu<span></span></a>
			</div>
		</header>
		<!--Fin Menu -->
	</section>
	<!-- Fin Header -->
		
		<?php

$link = mysqli_connect('127.0.0.1:52874', 'andoni', '1234', 'entic_users');
$sql = "SELECT * FROM usuarios";
mysqli_set_charset($link, "utf8");
$result = mysqli_query($link, $sql);

?>
      <div class="tablaPanel">
		<table class="table table-striped">
			<thead>
				<tr>
					<th style="text-align: center">ID Usuario</th>
					<th style="text-align: center">Username</th>
					<th style="text-align: center">Borrar Usuario</th>
				</tr>
			</thead>

			<tbody>
        
	<?php
while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    
    echo ("<tr>");
    echo ("<td>" . $row['id'] . "</td>");
    echo ("<td>" . $row['username'] . "</td>");
    echo ('<td><a type="button" href="eliminar.php?id='. $row['id'] .'"><button>Eliminar</button></a></td>');
    echo ("</tr>");
}


mysqli_free_result($result);

?>	
		</tbody>
		</table>
	</div>

		<section id="insertar section quote">
			<div class="container">
				<div class="col-md-8 col-md-offset-2 text-center">
					<a href="../Formulario/formularioInsertar.html" class="btn btn-large">Insertar usuario</a>
					<a href="../Formulario/clientes.php" class="btn btn-large">Ver Clientes</a>
				</div>
			</div>
		</section>
		
		
		  
		<!-- Nuestros JS -->
		<script
			src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="../js/bootstrap.min.js"></script>
		<script src="../js/jquery.flexslider-min.js"></script>
		<script src="../js/jquery.fancybox.pack.js"></script>
		<script src="../js/retina.min.js"></script>
		<script src="../js/modernizr.js"></script>
		<script src="../js/main.js"></script>

</body>
</html>

