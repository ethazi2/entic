<html>

<head>
<meta charset="utf-8">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Entic</title>

<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/flexslider.css">
<link rel="stylesheet" href="../css/jquery.fancybox.css">
<link rel="stylesheet" href="../css/main.css">
<link rel="stylesheet" href="../css/formu.css">
<link rel="stylesheet" href="../css/responsive.css">
<link rel="stylesheet" href="../css/font-icon.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
</head>

<body>
	<!-- Header -->
	<section class="bannerLog" role="banner">
		<!--Menu -->
		<header id="header">
			<div class="header-content clearfix">
				<a class="logo" href="../index.html"><img src="../images/logo4.png"
					alt=""></a>
				<nav class="navigation" role="navigation">
					<ul class="primary-nav">
						<li><a href="../index.html">Home</a></li>
						<li><a href="../html/Curriculums.html">Curriculums</a></li>
						<li><a href="Formulario.html">Trabaja con nosotros</a></li>
						<li><a href="../php/miCuenta.php">Mi Cuenta</a></li>
					</ul>
				</nav>
				<a href="#" class="nav-toggle">Menu<span></span></a>
			</div>
		</header>
		<!--Fin Menu -->
	</section>
	<!-- Fin Header -->
<?php
// datos base de datos
$host_db = "127.0.0.1:52874";
$user_db = "andoni";
$pass_db = "1234";
$db_name = "entic_users";

// conecxion a la base de datos
$conexion = new mysqli($host_db, $user_db, $pass_db, $db_name);

// Recibimos por POST los datos procedentes del formulario

$nombr = $_POST["nombre"];
$nombre = strip_tags($nombr); // Eliminamos la etiquetas que puedan existir
$n_nombre = strlen($nombre); // Contamos el numero de caracteres

$passwor = $_POST["passwordForm"];
$password = strip_tags($passwor); // Eliminamos la etiquetas que puedan existir
$n_password = strlen($password); // Contamos el numero de caracteres

$total_car = $n_nombre * $n_password; // Si alguno de ellos vale 0, $total_car valdr� 0

if ($total_car >= 1) {
    
    $_GRABAR_SQL = "INSERT INTO usuarios (username,password) VALUES ('$nombre','$password')";
    mysqli_query($conexion, $_GRABAR_SQL);
    
    // cerrar conexion BD
    mysqli_close($conexion);
    
    // Confirmamos que el registro ha sido insertado con exito
    
    echo "  
    <p><h3>Los datos han sido guardados con exito.</h3></p>  
      
    <p><a href='javascript:history.go(-2)'><button>VOLVER ATRAS</button></a></p>";
} else {
    echo "  
    Campos no rellenados.<br />  
    ";
}
?>  
<!-- Nuestros JS -->
		<script
			src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="../js/bootstrap.min.js"></script>
		<script src="../js/jquery.flexslider-min.js"></script>
		<script src="../js/jquery.fancybox.pack.js"></script>
		<script src="../js/retina.min.js"></script>
		<script src="../js/modernizr.js"></script>
		<script src="../js/main.js"></script>

</body>
</html>